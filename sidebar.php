<?php
global $grund;
$sidebar = $grund->the_query->get( 'sidebar', 'sidebar' );
?>
<div id="sidebar-$sidebar" class="sidebar">
    <?php dynamic_sidebar( $sidebar ); ?>
</div>
