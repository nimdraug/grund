<?php
global $grund, $grund_theme;

get_header();

if ( is_archive() || is_home() )
{
    ?><h1><?=$grund_theme->get_the_page_title()?></h1><?php
}

$grund->the_view();
get_footer();