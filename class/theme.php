<?php

class grund_theme
{
    function __construct()
    {
        global $grund, $wp_query;

        $grund->the_query = $wp_query;

        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue' ] );
        add_action( 'init', [ $this, 'init' ] );
    }

    function init()
    {
        // Add Theme Supports
        add_theme_support( 'custom-logo' );
        add_theme_support( 'post-thumbnails' );

        // Sidebars
        register_sidebar( [
            'name' => 'Sidebar',
            'id' => 'sidebar',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        ] );
    }

    function enqueue()
    {
        // Styles
        wp_enqueue_style( 'grund-styles', get_stylesheet_uri() );
    }

    function get_the_page_title()
    {
        global $s;

        if ( function_exists( 'is_tag' ) && is_tag() )
            return 'Tag Archive for &quot;' . $tag . '&quot;';
        elseif ( is_archive() )
            return wp_title( '', false ) . ' Archive';
        elseif ( is_search() )
            return 'Search for &quot;' . wp_specialchars( $s ) . '&quot;';
        elseif ( is_404() )
            return '404 Not Found';
        else
            return wp_title( '', false );
    }
}

$grund_theme = new grund_theme();
