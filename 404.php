<?php
global $post;

ob_start();
// 404 template content
?>
<h1>404 - Page not found</h1>
<p>The requested page could not be found.</p>
<?php

// create dummy post from content and add to query
$post = new WP_Post( (object)[ 'post_content' => ob_get_clean(), 'filter' => 'raw' ] );
$wp_query->posts[] = $post;
$wp_query->post_count = 1;

$grund->the_layout();
